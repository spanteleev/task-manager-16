package ru.tsc.panteleev.tm.component;

import ru.tsc.panteleev.tm.api.controller.ICommandController;
import ru.tsc.panteleev.tm.api.controller.IProjectController;
import ru.tsc.panteleev.tm.api.controller.IProjectTaskController;
import ru.tsc.panteleev.tm.api.controller.ITaskController;
import ru.tsc.panteleev.tm.api.repository.ICommandRepository;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.service.*;
import ru.tsc.panteleev.tm.constant.ArgumentConst;
import ru.tsc.panteleev.tm.constant.TerminalConst;
import ru.tsc.panteleev.tm.controller.CommandController;
import ru.tsc.panteleev.tm.controller.ProjectController;
import ru.tsc.panteleev.tm.controller.ProjectTaskController;
import ru.tsc.panteleev.tm.controller.TaskController;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.panteleev.tm.exception.system.CommandNotSupportedException;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.repository.CommandRepository;
import ru.tsc.panteleev.tm.repository.ProjectRepository;
import ru.tsc.panteleev.tm.repository.TaskRepository;
import ru.tsc.panteleev.tm.service.*;
import ru.tsc.panteleev.tm.util.DateUtil;
import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.util.Date;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService,projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    public void run(String[] args) {
        if (runWithArgument(args))
            System.exit(0);
        initData();
        initLogger();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                String command = TerminalUtil.nextLine();
                runWithCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void initData() {
        taskService.add(new Task("t5", Status.NOT_STARTED, DateUtil.toDate("04.12.2021")));
        taskService.add(new Task("t2", Status.IN_PROGRESS, DateUtil.toDate("04.12.2023")));
        taskService.add(new Task("t4", Status.COMPLETED, DateUtil.toDate("04.12.2022")));
        taskService.add(new Task("t1", Status.IN_PROGRESS, DateUtil.toDate("04.01.2023")));
        taskService.add(new Task("t3", Status.COMPLETED, DateUtil.toDate("04.12.2030")));
        projectService.add(new Project("p5", Status.COMPLETED,DateUtil.toDate("04.12.2023")));
        projectService.add(new Project("p1", Status.IN_PROGRESS,DateUtil.toDate("04.11.2022")));
        projectService.add(new Project("p4", Status.NOT_STARTED,DateUtil.toDate("01.12.2022")));
        projectService.add(new Project("p7", Status.IN_PROGRESS,DateUtil.toDate("04.11.2025")));
        projectService.add(new Project("p3", Status.NOT_STARTED,DateUtil.toDate("01.06.2024")));
    }

    public void runWithCommand(String command) {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedException();
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_LIST_SHOW_BY_PROJECT_ID:
                taskController.showTaskListByProjectId();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    public boolean runWithArgument(String[] args) {
        if (args == null || args.length == 0) return false;
        runWithArgument(args[0]);
        return true;
    }

    public void runWithArgument(String argument) {
        if (argument == null || argument.isEmpty()) throw new ArgumentNotSupportedException();
        switch (argument) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                throw new ArgumentNotSupportedException(argument);
        }
    }

    public static void exit() {
        System.exit(0);
    }

}
