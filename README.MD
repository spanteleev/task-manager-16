# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Sergey Panteleev

* **E-MAIL**: spanteleev@t1-consulting.ru

## SOFTWARE

* OpenJDK 8

* IntelliJ IDEA 2020.2.4

* Windows 10 21H1

## HARDWARE

* **RAM**: 32Gb

* **CPU**: Ryzen 5

* **SSD**: 512Gb

## BUILD APPLICATION

```shell script
mvn clean install
```


## RUN PROGRAM

```shell script
java -jar task-manager.jar
```

